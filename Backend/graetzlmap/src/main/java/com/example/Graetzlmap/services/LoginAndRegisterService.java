package com.example.Graetzlmap.services;
import com.example.Graetzlmap.dTOs.RegisterDTO;
import com.example.Graetzlmap.enums.Roles;
import com.example.Graetzlmap.model.Person;
import com.example.Graetzlmap.repositories.PersonCRUDRepository;
import com.example.Graetzlmap.validator.InputValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import javax.servlet.http.HttpServletRequest;

@Service
public class LoginAndRegisterService {
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    PersonCRUDRepository personCRUDRepository;
    @Autowired
    HttpServletRequest httpServletRequest;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    InputValidator validator;

    public Person register(RegisterDTO registerDTO) throws ResponseStatusException {
        try {
            validator.validateUsernameUniqueness(registerDTO.getUsername());
            validator.validateEmailUniqueness(registerDTO.getEmail());
            Person newUser = new Person(
                    registerDTO.getUsername(),
                    registerDTO.getEmail(),
                    passwordEncoder.encode(registerDTO.getPassword()),
                    Roles.ENDUSER);
            personCRUDRepository.save(newUser);

            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(registerDTO.getEmail(), registerDTO.getPassword());
            authToken.setDetails(new WebAuthenticationDetails(httpServletRequest));
            Authentication authentication = authenticationManager.authenticate(authToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            httpServletRequest.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());
            newUser.setPassword(null);
            return newUser;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw e;
        }
    }

    public Person login(){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if(username == null || username.equals("anonymousUser"))
            return null;
        Person person = personCRUDRepository.findByUsername(username);
        person.setPassword(null);
        return person;
    }
}