package com.example.Graetzlmap.dummyData;

import com.example.Graetzlmap.enums.Categories;

public class DummyMapPostDTO {
    private String title;
    private String text;
    private String picturePath;
    private Categories category;
    private Integer postalCode;
    private double latitude;
    private double longitude;
    private String username;


    public DummyMapPostDTO(){}

    public DummyMapPostDTO(String title, String text, String picturePath, Categories category, Integer postalCode, double latitude, double longitude, String username) {
        this.title = title;
        this.text = text;
        this.picturePath = picturePath;
        this.category = category;
        this.postalCode = postalCode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.username = username;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public Categories getCategory() {
        return category;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
