package com.example.Graetzlmap.services;
import com.example.Graetzlmap.model.MapPost;
import com.example.Graetzlmap.model.Person;
import com.example.Graetzlmap.model.PostRating;
import com.example.Graetzlmap.repositories.MapPostCRUDRepository;
import com.example.Graetzlmap.repositories.PersonCRUDRepository;
import com.example.Graetzlmap.repositories.PostRatingCRUDRepository;
import com.example.Graetzlmap.validator.InputValidator;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;

@Transactional
@Service
public class PostRatingService {
    @Autowired
    PostRatingCRUDRepository postRatingCRUDRepository;
    @Autowired
    MapPostCRUDRepository mapPostCRUDRepository;
    @Autowired
    PersonCRUDRepository personCRUDRepository;
    @Autowired
    InputValidator validator;

    public MapPost ratePost(@NotNull Boolean rating, Integer id) throws ResponseStatusException {
        String username = validator.getCurrentUsername();
        validator.validateUsername(username);
        validator.validateMapPostExistence(id);
        if(!postRatingCRUDRepository.existsByPersonIdAndPostId(personCRUDRepository.findByUsername(username).getId(),id)) {
            if(rating == null)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "no rating is given.");
            return createNewPostRating(rating, id);
        }
        else
            return updatePostRating(rating, id, username);
    }

    public MapPost createNewPostRating(@NotNull Boolean rating, Integer id) throws ResponseStatusException {
        MapPost mapPost = mapPostCRUDRepository.findById(id).get();
        Person person =  personCRUDRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        PostRating postRating = new PostRating(rating,id , person.getId());
        if (postRating.isRating())
            mapPost.addLike();
        if (!postRating.isRating())
            mapPost.addDislikes();
        postRating.setPerson(person);
        postRating.setPost(mapPost);
        person.addPostRatingSet(postRating);
        mapPost.addPostRating(postRating);
        postRatingCRUDRepository.save(postRating);
        return mapPost;
    }

    public MapPost updatePostRating(Boolean rating, Integer id, String username) throws ResponseStatusException{
        MapPost mapPost = mapPostCRUDRepository.findById(id).get();
        Person person =  personCRUDRepository.findByUsername(username);
        if(!postRatingCRUDRepository.existsByPersonIdAndPostId(person.getId(), mapPost.getId()))
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "error occured during rating creation");
        PostRating oldRating = postRatingCRUDRepository.findByPersonIdAndPostId(person.getId(), mapPost.getId());
        if(rating == null){
            if(!oldRating.isRating())
                mapPost.deleteDislike();
            else
                mapPost.deleteLike();
            postRatingCRUDRepository.delete(oldRating);
        }
        else if(!rating && oldRating.isRating()){
                mapPost.deleteLike();
                mapPost.addDislikes();
                oldRating.setRating(rating);
                postRatingCRUDRepository.save(oldRating);
        }
        else if(rating && !oldRating.isRating()){
                mapPost.addLike();
                mapPost.deleteDislike();
                oldRating.setRating(rating);
                postRatingCRUDRepository.save(oldRating);
        }
        else if(rating && oldRating.isRating()){
            mapPost.deleteLike();
            postRatingCRUDRepository.delete(oldRating);
        }
        else if(!rating && !oldRating.isRating()){
            mapPost.deleteDislike();
            postRatingCRUDRepository.delete(oldRating);
        }
        return mapPost;
    }

    public List<PostRating> allPostRatings(){
        return (List<PostRating>) postRatingCRUDRepository.findAll();}

    public Boolean fetchPostRating(Integer id){
        String username = validator.getCurrentUsername();
        Person person = personCRUDRepository.findByUsername(username);
        if(postRatingCRUDRepository.existsByPersonIdAndPostId(person.getId(), id))
            return postRatingCRUDRepository.findByPersonIdAndPostId(person.getId(), id).isRating();
        else
            return null;
    }

}
