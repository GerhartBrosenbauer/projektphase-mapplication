package com.example.Graetzlmap.exitDTOs;
import com.example.Graetzlmap.enums.Categories;
import com.example.Graetzlmap.model.CommentOfPost;
import com.example.Graetzlmap.model.MapPost;
import java.time.LocalDateTime;
import java.util.List;

public class MapPostWithCommentsDTO {
    private int id;
    private String title;
    private String text;
    private String image;
    private boolean approvalStatus;
    private String approvalModerator;
    private int likes;
    private int dislikes;
    private Categories category;
    private LocalDateTime creationDate;
    private Integer postalCode;
    private double latitude;
    private double longitude;
    private List<CommentOfPost> commentOfPostList;

    public MapPostWithCommentsDTO(MapPost mapPost, List<CommentOfPost> commentOfPostList){
        this.id = mapPost.getId();
        this.title = mapPost.getTitle();
        this.text = mapPost.getText();
        this.image = mapPost.getImage();
        this.approvalStatus = mapPost.isApprovalStatus();
        this.approvalModerator = mapPost.getApprovalModerator();
        this.likes = mapPost.getLikes();
        this.dislikes = mapPost.getDislikes();
        this.category = mapPost.getCategory();
        this.creationDate = mapPost.getCreationDate();
        this.postalCode = mapPost.getPostalCode();
        this.latitude = mapPost.getLatitude();
        this.longitude = mapPost.getLongitude();
        this.commentOfPostList = commentOfPostList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(boolean approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalModerator() {
        return approvalModerator;
    }

    public void setApprovalModerator(String approvalModerator) {
        this.approvalModerator = approvalModerator;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getDislikes() {
        return dislikes;
    }

    public void setDislikes(int dislikes) {
        this.dislikes = dislikes;
    }

    public Categories getCategory() {
        return category;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public List<CommentOfPost> getCommentOfPostList() {
        return commentOfPostList;
    }

    public void setCommentOfPostList(List<CommentOfPost> commentOfPostList) {
        this.commentOfPostList = commentOfPostList;
    }
}
