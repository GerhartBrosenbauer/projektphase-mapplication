package com.example.Graetzlmap.model;
import com.example.Graetzlmap.enums.Categories;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity(name = "newsPost")
@Table
public class NewsPost extends Post {

    public NewsPost(){}

    public NewsPost(String title, String text, boolean approvalStatus, Categories category, LocalDateTime creationDate, Integer postalCode, double latitude, double longitude){
        super(title, text, category, creationDate, postalCode, latitude, longitude);
        this.setApprovalStatus(true);

    }
}
