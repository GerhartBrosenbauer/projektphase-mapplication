package com.example.Graetzlmap.model;
import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.*;

@Entity(name = "postRating")
@Table
public class PostRating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private Boolean rating;

    @Column
    private int postId;

    @Column
    private int personId;

    //  RELATIONS

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "Rating_Person")
    private Person person;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "Relating_Post")
    private Post post;

    //  CONSTRUCTOR

    public PostRating(){}

    public PostRating(Boolean rating, int postId, int personId){this.rating = rating; this.postId=postId; this.personId=personId;}

    //  GETTER & SETTER

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean isRating() {
        return rating;
    }

    public void setRating(Boolean rating) {
        this.rating = rating;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    //  RELATIONS GETTER & SETTER & ADDER

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
