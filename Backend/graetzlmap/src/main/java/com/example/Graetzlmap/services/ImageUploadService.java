package com.example.Graetzlmap.services;
import com.example.Graetzlmap.model.MapPost;
import com.example.Graetzlmap.model.Person;
import com.example.Graetzlmap.repositories.MapPostCRUDRepository;
import com.example.Graetzlmap.repositories.PersonCRUDRepository;
import com.example.Graetzlmap.utilities.FileUploadUtil;
import com.example.Graetzlmap.validator.InputValidator;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImageUploadService {
    @Autowired
    PersonCRUDRepository personCRUDRepository;
    @Autowired
    MapPostCRUDRepository mapPostCRUDRepository;
    @Autowired
    InputValidator validator;

    public Person saveProfilePicture(@NotNull MultipartFile multipartFile) throws Exception{
        try {
            String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename()).replaceAll(" ", "");
            String username = validator.getCurrentUsername();
            Person person = personCRUDRepository.findByUsername(username);
            person.setImage(fileName);
            personCRUDRepository.save(person);
            String uploadDir = "person-photos/" + person.getId();
            FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
            Person person1 = personCRUDRepository.findById(person.getId()).get();
            person1.setPassword(null);
            return person1;
        }catch (Exception e){
            e.getMessage();
            throw e;
        }
    }

    public void savePostPicture(@NotNull MultipartFile multipartFile, int postId) throws Exception{
        try {
            String fileName = StringUtils.cleanPath((multipartFile.getOriginalFilename())).replaceAll(" ", "");
            validator.validateMapPostExistence(postId);
            MapPost mapPost = mapPostCRUDRepository.findById(postId).get();
            mapPost.setImage(fileName);
            mapPostCRUDRepository.save(mapPost);
            String uploadDir = "post-photos/" + mapPost.getId();
            FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
        }catch (Exception e){
            e.getMessage();
            throw e;
        }
    }
}
