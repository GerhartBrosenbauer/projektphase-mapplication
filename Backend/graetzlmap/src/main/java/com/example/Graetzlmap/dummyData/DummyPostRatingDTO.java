package com.example.Graetzlmap.dummyData;

public class DummyPostRatingDTO {
    private Boolean rating;
    private int postId;
    private String username;

    public DummyPostRatingDTO(Boolean rating, int postId,String username) {
        this.rating = rating;
        this.username = username;
        this.postId = postId;
    }

    public Boolean isRating() {
        return rating;
    }

    public void setRating(Boolean rating) {
        this.rating = rating;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
