package com.example.Graetzlmap.controller;
import com.example.Graetzlmap.exitDTOs.MapPostWithCommentsDTO;
import com.example.Graetzlmap.model.CommentOfPost;
import com.example.Graetzlmap.model.MapPost;
import com.example.Graetzlmap.repositories.CommentOfPostCRUDRepository;
import com.example.Graetzlmap.services.CommentOfPostService;
import com.example.Graetzlmap.services.MapPostService;
import com.example.Graetzlmap.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/admin")
public class AdminController {
    @Autowired
    PersonService personService;
    @Autowired
    MapPostService mapPostService;
    @Autowired
    CommentOfPostService commentOfPostService;
    @Autowired
    CommentOfPostCRUDRepository commentOfPostCRUDRepository;

    //  TITLE                   Role change-Requests

    @PutMapping("/personToEndUser/{username}")
    public void personToEndUser(@PathVariable String username) throws ResponseStatusException {personService.personToEndUser(username);}

    @PutMapping("/personToModerator/{username}")
    public void personToModerator(@PathVariable String username) throws ResponseStatusException{personService.personToModerator(username);}

    @PutMapping("/personToAdmin/{username}")
    public void personToAdmin(@PathVariable String username) throws ResponseStatusException{personService.personToAdmin(username);}

    //  TITLE                   Post/Comment approval

    @PutMapping("/postApproval/{id}")
    public void postApproval(@PathVariable Integer id) throws ResponseStatusException{mapPostService.changeApproval(id);}

    @PutMapping("/commentApproval/{id}")
    public boolean commentApproval(@PathVariable Integer id) throws ResponseStatusException{
        commentOfPostService.changeApproval(id);
        return commentOfPostCRUDRepository.findById(id).get().isApprovalStatus();
    }

    //  TITLE                   Unapproved Post fetching

    @GetMapping("/allIllegalPosts")
    public List<MapPost> allIllegalPosts() { return mapPostService.findAllIllegalPosts(); }

    @GetMapping("/allIllegalComments")
    public List<CommentOfPost> allIllegalComments() { return commentOfPostService.fetchIllegalComments();}

    @GetMapping("/postWithAllComments/{id}")
    public MapPostWithCommentsDTO getRawMapPost(@PathVariable Integer id) throws ResponseStatusException{
        return mapPostService.getMapPostAllComments(id);
    }
}
