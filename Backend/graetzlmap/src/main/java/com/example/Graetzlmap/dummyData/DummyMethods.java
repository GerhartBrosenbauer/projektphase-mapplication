package com.example.Graetzlmap.dummyData;

import com.example.Graetzlmap.enums.Categories;
import com.example.Graetzlmap.enums.Roles;
import com.example.Graetzlmap.services.MapPostService;
import com.example.Graetzlmap.services.PersonService;
import com.example.Graetzlmap.model.CommentOfPost;
import com.example.Graetzlmap.model.MapPost;
import com.example.Graetzlmap.model.Person;
import com.example.Graetzlmap.model.PostRating;
import com.example.Graetzlmap.repositories.CommentOfPostCRUDRepository;
import com.example.Graetzlmap.repositories.MapPostCRUDRepository;
import com.example.Graetzlmap.repositories.PersonCRUDRepository;
import com.example.Graetzlmap.repositories.PostRatingCRUDRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.parameters.P;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Stream;

@Service
@Transactional
public class DummyMethods {

    @Autowired
    PersonCRUDRepository personCRUDRepository;
    @Autowired
    PersonService personService;
    @Autowired
    MapPostCRUDRepository mapPostCRUDRepository;
    @Autowired
    CommentOfPostCRUDRepository commentOfPostCRUDRepository;
    @Autowired
    PostRatingCRUDRepository postRatingCRUDRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    MapPostService mapPostService;

    private final Random random = new Random();

    public void newMapPost(@NotNull DummyMapPostDTO newMapPostDTO) throws ResponseStatusException {
        if(!personCRUDRepository.existsByUsername(newMapPostDTO.getUsername()))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found");
        try {
            LocalDateTime date = LocalDateTime.now();
            MapPost mapPost = new MapPost(
                    newMapPostDTO.getTitle(),
                    newMapPostDTO.getText(),
                    newMapPostDTO.getCategory(),
                    date,
                    newMapPostDTO.getPostalCode(),
                    newMapPostDTO.getLatitude(),
                    newMapPostDTO.getLongitude()
            );
            if(newMapPostDTO.getPicturePath() != null && newMapPostDTO.getPicturePath() != "")
                mapPost.setImage(newMapPostDTO.getPicturePath());
            Person person = personService.findPerson(newMapPostDTO.getUsername());
            if(person.getRole() != Roles.ENDUSER)
                mapPost.setApprovalStatus(true);
            mapPostCRUDRepository.save(mapPost);
            MapPost mapPost1 = mapPostCRUDRepository.findById(mapPost.getId()).get();
            person.addPostSet(mapPost1);
            mapPost.setPerson(person);
            mapPostCRUDRepository.save(mapPost);
            personCRUDRepository.save(person);

        }catch (Exception e){
            System.out.println(e.getMessage());;
        }
    }

    public void createUnaprovedComments(){

        for(MapPost mapPost : mapPostService.findAll()) {
            createNewCommentOfPost(new DummyCommentOfPostDTO("Das ist ein nicht-positives Kommentar", "KatharinaPersona", mapPost.getId()));
            createNewCommentOfPost(new DummyCommentOfPostDTO("Das ist ein nicht-positives Kommentar", "OtmarPersona", mapPost.getId()));

        }

    }


    public void createNewCommentOfPost(@NotNull DummyCommentOfPostDTO commentOfPostDTO) throws ResponseStatusException{
        if(!personCRUDRepository.existsByUsername(commentOfPostDTO.getUsername()))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found");
        if(!mapPostCRUDRepository.existsById(commentOfPostDTO.getPostId()))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "post not found");
        try{
            LocalDateTime date = LocalDateTime.now();
            boolean approval = true;
            if(personService.findPerson(commentOfPostDTO.getUsername()).getRole() == Roles.ENDUSER)
                approval = false;
            CommentOfPost commentOfPost = new CommentOfPost(commentOfPostDTO.getText(), approval, date);
            commentOfPostCRUDRepository.save(commentOfPost);
            MapPost mapPost = mapPostCRUDRepository.findById(commentOfPostDTO.getPostId()).get();
            commentOfPost.setPost(mapPost);
            mapPost.addCommentOfPost(commentOfPost);
            mapPostCRUDRepository.save(mapPost);
            Person person = personService.findPerson(commentOfPostDTO.getUsername());
            commentOfPost.setPerson(person);
            person.addCommentOfPostSet(commentOfPost);
            personCRUDRepository.save(person);
            commentOfPostCRUDRepository.save(commentOfPost);
        }catch (Exception e) {
            e.getMessage();
        }
    }

    public void personaDummyPosts(){
        personCRUDRepository.save(new Person("KatharinaPersona", "bwl4life@bwl.com", passwordEncoder.encode("dorwssap"), Roles.ENDUSER));
        personCRUDRepository.save(new Person("OtmarPersona", "kingOtmar@gmx.com", passwordEncoder.encode("Auto3"), Roles.ENDUSER));
        personCRUDRepository.save(new Person("KevinPersona", "kevin.alleine@zuhause.at", passwordEncoder.encode("ilovemySon1"), Roles.ENDUSER));
        personCRUDRepository.save(new Person("NatashaPersona", "NataschaWarNichtDa@gmail.com", passwordEncoder.encode("JavaScript=pepeHands"), Roles.ENDUSER));
        DummyMapPostDTO newMapPostDTO = new DummyMapPostDTO("Ich hasse Autos", "Jeden Tag wenn ich auf dem Weg zur Uni bin, wird meine Lunge von den Abgasen der Autos belästigt. Dies kann ich nicht mehr dulden und werde jede Sekunde meiner Lebenszeit damit verwenden Autos zu verbannen! MUHAHAHAHAHAHAHA!", "", Categories.VERKEHRSSICHERHEIT, 1140, randomLatitude(), randomLongitude(), "KatharinaPersona");
        newMapPost(newMapPostDTO);
        DummyMapPostDTO newMapPostDTO1 = new DummyMapPostDTO("Das einzig grüne in Wien ist Salat", "Leute. Wenn ich in einer grauen Stadt leben will, würde ich nach Salzburg ziehen. Ich will endlich Blumenbeete in der Stadt sehen!!!", "", Categories.GRÜNFLÄCHEN, 1140, randomLatitude(), randomLongitude(), "OtmarPersona");
        newMapPost(newMapPostDTO1);
        DummyMapPostDTO newMapPostDTO2 = new DummyMapPostDTO("Wien ist !gut", "Leute was ist in dieser Stadt eigentlich los?! Keine Spielplätze, kein grün, überall Autos. Meine Tochter kann die Stadt besser planen.", "", Categories.SONSTIGES, 1140, randomLatitude(), randomLongitude(), "KevinPersona");
        newMapPost(newMapPostDTO2);
        DummyMapPostDTO newMapPostDTO3 = new DummyMapPostDTO("Kevin postet zu viel hier", "Liebe Leute,  mein Ehemann Kevin regt sich dauernt hier über Wien auf. Er meint es nicht so. Bannt ihn einfach.  Mit freundlichen Grüßen,  Natscha Kevinitis ", "", Categories.SONSTIGES, 1140, randomLatitude(), randomLongitude(), "NatashaPersona");
        newMapPost(newMapPostDTO3);
    }

    public void dummyPostCreation(){
        for(Person person : personService.findAll()){
            DummyMapPostDTO newMapPostDTO4 = new DummyMapPostDTO("Ich hasse Autos1", "Jeden Tag wenn ich auf dem Weg zur Uni bin, wird meine Lunge von den Abgasen der Autos belästigt. Dies kann ich nicht mehr dulden und werde jede Sekunde meiner Lebenszeit damit verwenden Autos zu verbannen! MUHAHAHAHAHAHAHA!", "", Categories.VERKEHRSSICHERHEIT, 1140, randomLatitude(), randomLongitude(), person.getUsername());
            newMapPost(newMapPostDTO4);
        }
    }

    public void createAccurateDummyPosts(){
        DummyMapPostDTO newMapPostDTO1 = new DummyMapPostDTO("Coole Schule hier", "Der tolle Lehrer namens Daniel hat mir gezeigt warum Javascript Frameworks braucht damit man es verwenden kann, ohne den Verstand zu verlieren", "schule.jpg", Categories.AKTIVITÄTEN, 1110, 48.18492677566418, 16.420745323714208, "Gerti");
        newMapPost(newMapPostDTO1);
        DummyMapPostDTO newMapPostDTO2 = new DummyMapPostDTO("Schlagloch im Boden", "Seit etwa einem Monat befindet sich hier ein Schlagloch. Mein Fahrrad hatte einen Platten deswegen. Wäre toll wenn das repariert wird!", "loch.jpg", Categories.VERKEHRSSICHERHEIT, 1140, 48.18957954338458, 16.285205959130167, "Elmo");
        newMapPost(newMapPostDTO2);
        DummyMapPostDTO newMapPostDTO3 = new DummyMapPostDTO("Nicht genug Sushi - Läden hier", "Ich bin bekannt als Sushi Connoisseur. Ein wahrhaftinger Sushi - Aficionado , leider ist hier nur ein Sushi-Restaurant und mit dem kann ich nicht leben", "", Categories.SONSTIGES, 1110, 48.18515189927592, 16.418356056855806, "Señor Joñas Schwarr");
        newMapPost(newMapPostDTO3);
        DummyMapPostDTO newMapPostDTO4 = new DummyMapPostDTO("Zu nass!", "Ich weiß das ist die Donau, aber könnten wir vielleicht ein bisschen auf die Umwelt achten und das Wasser entfernen und zu Orten bringen, wo dieses wirklich gebraucht wird?", "donau.jpg", Categories.AKTIVITÄTEN, 1210, 48.266433720919196, 16.366674829626604, "Meister Tertl");
        newMapPost(newMapPostDTO4);
        DummyMapPostDTO newMapPostDTO5 = new DummyMapPostDTO("Achtung! Autogefahr!", "Die Autos fahren echt nah am Gehsteig, und es gibt keine Sicherung für die Fußgänger. Das ist sehr gefährlich! Wer denk an die Kinder?!", "cars.jpg", Categories.VERKEHRSSICHERHEIT, 1010, 48.209624112643255, 16.371778317901946, "AlanTuring");
        newMapPost(newMapPostDTO5);
        DummyMapPostDTO newMapPostDTO6 = new DummyMapPostDTO("Hier könnte man super ein Blumenbeet hinmachen", "Wer will schon in einer grauen Stadt leben? Niemand! Deshalb wäre es super, wenn wir hier ein Blumenbeet hinmachen.", "pit.jpg", Categories.GRÜNFLÄCHEN, 1120, 48.176549786891314, 16.331576781635047, "JuiceWorld");
        newMapPost(newMapPostDTO6);
        DummyMapPostDTO newMapPostDTO7 = new DummyMapPostDTO("Ein Gehweg wäre super hier!", "Jeden Tag, wenn ich zur Arbeit gehe, werde ich fast überfahren, weil es hier keinen Fußgängerweg gibt. Aber es gibt auch keine andere Möglichkeit an mein Ziel zu kommen. Ein Gehweg wäre super hier!!", "weg.jpg", Categories.WEGE, 1120, 48.247941487864544, 16.44674037670901, "StefanKaiser");
        newMapPost(newMapPostDTO7);
        DummyMapPostDTO newMapPostDTO8 = new DummyMapPostDTO("Hier könnte man super Schweine züchten", "Ich hab als Kind immer Winnie Pooh gesehen, und will mein eigenes Ferkel. Keines der Schweine soll geschlachtet werden, ich will sie nur ansehen uns füttern", "ferkel.jpg", Categories.NUTZFLÄCHEN, 1160, 48.21574446171893, 16.310409887984733, "SimonGarfunkle");
        newMapPost(newMapPostDTO8);
    }

    public void commentPosts(){
        int counter = 0;
        for(MapPost mapPost : mapPostService.findAll()){
            if(mapPost.getId() == 7){
                createNewCommentOfPost(new DummyCommentOfPostDTO("Mei Bier is ned deppat", "Gerti", mapPost.getId()));
            }
            if(mapPost.isApprovalStatus()){
                for(Person person : personService.findAll()) {
                    if(counter == 5){
                        counter = 0;
                        break;
                    }
                    switch(counter){
                        case 0 -> {createNewCommentOfPost(new DummyCommentOfPostDTO("YEP YEP YEP 1000%!!!!", person.getUsername(), mapPost.getId()));}
                        case 1 -> {createNewCommentOfPost(new DummyCommentOfPostDTO("Gute Idee!", person.getUsername(), mapPost.getId()));}
                        case 2 -> {createNewCommentOfPost(new DummyCommentOfPostDTO("Interessant", person.getUsername(), mapPost.getId()));}
                        case 3 -> {createNewCommentOfPost(new DummyCommentOfPostDTO("Ich finde das nicht so gut :/", person.getUsername(), mapPost.getId()));}
                        case 4 -> {createNewCommentOfPost(new DummyCommentOfPostDTO("Das sollten wir echt machen!", person.getUsername(), mapPost.getId()));}
                    }
                    counter ++;
                }
            }
        }
    }

    public void randomLikingDisliking(){
        boolean rating = false;
        for(Person person : personService.findAll()){
            for(MapPost mapPost : mapPostCRUDRepository.findAll()){
                if (random.nextBoolean()) {
                    rating = true;
                } else {
                    rating = false;
                }
                if(mapPost.getId() == 3)
                    rating = false;
                if(mapPost.getId() == 8)
                    rating = true;
                DummyPostRatingDTO dummyPostRatingDTO = new DummyPostRatingDTO(rating, mapPost.getId(), person.getUsername());
                createNewPostRating(dummyPostRatingDTO);
            }
        }
    }

    private Double randomLatitude(){
        Random random = new Random();
        int max = 48260000;
        int min = 48140000;
        int randomNum = random.nextInt(max) % (max - min + 1) + min;
        return ((double)randomNum)/1000000;
    }

    private Double randomLongitude(){
        Random random = new Random();
        int max = 16500000;
        int min = 16270000;
        int randomNum = random.nextInt(max) % (max - min + 1) + min;
        return ((double)randomNum)/1000000;
    }

    public void generate3Admins(){
        personCRUDRepository.save(new Person("Señor Joñas Schwarr","admin@admin.com", passwordEncoder.encode("passwort"), Roles.ADMIN));
        personCRUDRepository.save(new Person("Gerti","g.brosenbauer@protonmail.com", passwordEncoder.encode("ilovejava123"), Roles.ADMIN));
        personCRUDRepository.save(new Person("Elmo","rom@mann.com", passwordEncoder.encode("passwort"), Roles.ADMIN));
    }

    public void generateMods(){
        personCRUDRepository.save(new Person("SimonGarfunkle","simon@garfunkel.com", passwordEncoder.encode("passwort"), Roles.MODERATOR));
        personCRUDRepository.save(new Person("M&M","malcom@mccormick.com", passwordEncoder.encode("passwort"), Roles.MODERATOR));
        personCRUDRepository.save(new Person("JuiceWorld","juice@world.com", passwordEncoder.encode("passwort"), Roles.MODERATOR));
        personCRUDRepository.save(new Person("AlanTuring","alan@turing.com", passwordEncoder.encode("passwort"), Roles.MODERATOR));
        personCRUDRepository.save(new Person("StefanKaiser","stefan@kaiser.com", passwordEncoder.encode("passwort"), Roles.MODERATOR));
        personCRUDRepository.save(new Person("Meister Tertl","tert@memel.com", passwordEncoder.encode("passwort"), Roles.MODERATOR));
        personCRUDRepository.save(new Person("CaptainMurphy","captain@murphy.com", passwordEncoder.encode("passwort"), Roles.MODERATOR));
    }

    public void generateEndUser(){
        for (int i = 0; i <= 50; i++) {
            String vorname = "";
            String nachname = "";
            String benutzername = "";
            int zufallVorname = (int) (Math.random() * 11);
            int zufallNachname = (int) (Math.random() * 11);
            switch (zufallVorname) {
                case 0:
                    vorname = "Emma";
                    break;
                case 1:
                    vorname = "Michael";
                    break;
                case 2:
                    vorname = "Marco";
                    break;
                case 3:
                    vorname = "Sara";
                    break;
                case 4:
                    vorname = "Lisa";
                    break;
                case 5:
                    vorname = "Peter";
                    break;
                case 6:
                    vorname = "Moritz";
                    break;
                case 7:
                    vorname = "Felix";
                    break;
                case 8:
                    vorname = "Susanne";
                    break;
                case 9:
                    vorname = "Mia";
                    break;
                case 10:
                    vorname = "Simon";
                    break;
            }
            switch (zufallNachname) {
                case 0:
                    nachname = "Mustermann";
                    break;
                case 1:
                    nachname = "Musterfrau";
                    break;
                case 2:
                    nachname = "Müller";
                    break;
                case 3:
                    nachname = "Rosenrot";
                    break;
                case 4:
                    nachname = "Bauer";
                    break;
                case 5:
                    nachname = "Weber";
                    break;
                case 6:
                    nachname = "Hoffmann";
                    break;
                case 7:
                    nachname = "Hemmer";
                    break;
                case 8:
                    nachname = "Leithner";
                    break;
                case 9:
                    nachname = "Brunner";
                    break;
                case 10:
                    nachname = "Vogel";
                    break;
            }
            personCRUDRepository.save(new Person(vorname + i, "EnduserMail" + i, passwordEncoder.encode("password"), Roles.ENDUSER));
        }
    }

    public void ratePost(@NotNull DummyPostRatingDTO postRatingDTO) throws ResponseStatusException {

        if(!personCRUDRepository.existsByUsername(postRatingDTO.getUsername()))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found");
        if(!mapPostCRUDRepository.existsById(postRatingDTO.getPostId()))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "post not found");
        if(!postRatingCRUDRepository.existsByPersonIdAndPostId(personCRUDRepository.findByUsername(postRatingDTO.getUsername()).getId(),postRatingDTO.getPostId())) {
            if(postRatingDTO.isRating() == null)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "no rating is given.");
            createNewPostRating(postRatingDTO);
        }
        else
            updatePostRating(postRatingDTO);
    }

    public void createNewPostRating(@NotNull DummyPostRatingDTO postRatingDTO) throws ResponseStatusException {
        MapPost mapPost = mapPostCRUDRepository.findById(postRatingDTO.getPostId()).get();
        Person person =  personCRUDRepository.findByUsername(postRatingDTO.getUsername());
        PostRating postRating = new PostRating(postRatingDTO.isRating(), postRatingDTO.getPostId(), person.getId());
        if (postRating.isRating())
            mapPost.addLike();
        if (!postRating.isRating())
            mapPost.addDislikes();
        postRating.setPerson(person);
        postRating.setPost(mapPost);
        person.addPostRatingSet(postRating);
        mapPost.addPostRating(postRating);
        postRatingCRUDRepository.save(postRating);

    }

    public void updatePostRating(@NotNull DummyPostRatingDTO postRatingDTO) throws ResponseStatusException{
        MapPost mapPost = mapPostCRUDRepository.findById(postRatingDTO.getPostId()).get();
        Person person =  personCRUDRepository.findByUsername(postRatingDTO.getUsername());
        if(!postRatingCRUDRepository.existsByPersonIdAndPostId(person.getId(), mapPost.getId()))
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "error occured during rating creation");
        PostRating oldRating = postRatingCRUDRepository.findByPersonIdAndPostId(person.getId(), mapPost.getId());
        if(postRatingDTO.isRating() == null){
            if(!oldRating.isRating())
                mapPost.deleteDislike();
            else
                mapPost.deleteLike();
            postRatingCRUDRepository.delete(oldRating);
        }
        else if(!postRatingDTO.isRating() && oldRating.isRating()){
            mapPost.deleteLike();
            mapPost.addDislikes();
        }
        else if(postRatingDTO.isRating() && !oldRating.isRating()){
            mapPost.addLike();
            mapPost.deleteDislike();
        }
        if(postRatingDTO.isRating() != null) {
            oldRating.setRating(postRatingDTO.isRating());
            postRatingCRUDRepository.save(oldRating);
        }
    }
}
