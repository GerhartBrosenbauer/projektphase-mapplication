package com.example.Graetzlmap.controller;
import com.example.Graetzlmap.dTOs.PostRatingDTO;
import com.example.Graetzlmap.model.MapPost;
import com.example.Graetzlmap.model.PostRating;
import com.example.Graetzlmap.services.PostRatingService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/postRating")
public class RatingController {
    @Autowired
    PostRatingService postRatingService;

    @PostMapping("")
    public MapPost ratePost(@RequestBody @NotNull PostRatingDTO postRatingDTO) throws ResponseStatusException {
        return postRatingService.ratePost(postRatingDTO.isRating(), postRatingDTO.getPostId());}

    @GetMapping("/all")
    public List<PostRating> returnAllPostRatings() throws ResponseStatusException{return postRatingService.allPostRatings();}

    @GetMapping("/{id}")
    public Boolean fetchPostRating(@PathVariable Integer id){
        return postRatingService.fetchPostRating(id);
    }
}
