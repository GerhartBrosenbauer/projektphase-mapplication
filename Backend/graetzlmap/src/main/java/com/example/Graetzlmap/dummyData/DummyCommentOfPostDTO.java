package com.example.Graetzlmap.dummyData;

public class DummyCommentOfPostDTO {

    private String text;
    private String username;
    private int postId;


    public DummyCommentOfPostDTO(String text, String username, int postId) {
        this.text = text;
        this.username = username;
        this.postId = postId;

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
