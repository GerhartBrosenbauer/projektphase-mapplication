package com.example.Graetzlmap.repositories;

import com.example.Graetzlmap.model.NewsPost;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsPostCRUDRepository extends CrudRepository<NewsPost, Integer> {
}
