package com.example.Graetzlmap.repositories;

import com.example.Graetzlmap.model.MapPost;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MapPostCRUDRepository extends CrudRepository<MapPost, Integer> {
    void deleteById(Integer id);

    //@Query("SELECT mp.id, mp.approval_moderator, mp.approval_status, mp.category, mp.creation_date, mp.likes, mp.dislikes, mp.latitude, mp.longitude, mp.picturePath, mp.postal_code, mp.text, mp.title FROM mappost mp")
    @Query("SELECT DISTINCT mp.title, p  FROM mapPost as mp LEFT JOIN mp.person as p where mp.approvalStatus = 1 ")
    List<Object> mapPostsWithoutComments();
}
