package com.example.Graetzlmap.model;
import com.example.Graetzlmap.enums.Roles;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "person")
@Table
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(unique = true)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column
    private Integer postalCode;

    @Column
    private Roles role;

    @Column
    private String image;

    //  RELATIONS

    @JsonBackReference
    @OneToMany(mappedBy = "person", cascade = CascadeType.REMOVE)
    private Set<CommentOfPost> commentOfPostSet = new HashSet<>();

    @JsonBackReference
    @OneToMany(mappedBy = "person", cascade = CascadeType.REMOVE)
    private Set<PostRating> postRatingSet = new HashSet<>();

    @JsonBackReference
    @OneToMany(mappedBy = "person", cascade = CascadeType.REMOVE)
    private Set<Post> postSet = new HashSet<>();

    //  CONSTRUCTOR

    public Person(){}

    public Person(String username, String email, String password, Roles role) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.role= role;
    }

    // GETTER & SETTER

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        if(image == null || image == "")
            return "http://localhost:8080/standart-person-photos/jonass.jpg";
        return "http://localhost:8080/person-photos/" + id + "/" + image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

//  RELATIONS GETTER & SETTER & ADDER


   public Set<CommentOfPost> getCommentOfPostSet() {
       return commentOfPostSet;
   }

   public void setCommentOfPostSet(Set<CommentOfPost> commentOfPostSet) {
       this.commentOfPostSet = commentOfPostSet;
   }

   public void addCommentOfPostSet(CommentOfPost commentOfPost) {
       this.commentOfPostSet.add(commentOfPost);
   }

    public Set<PostRating> getPostRatingSet() {
        return postRatingSet;
    }

    public void setPostRatingSet(Set<PostRating> postRatingSet) {
        this.postRatingSet = postRatingSet;
    }

    public void addPostRatingSet(PostRating postRating) {
        this.postRatingSet.add(postRating);
    }

    public Set<Post> getPostSet() {
        return postSet;
    }

    public void setPostSet(Set<Post> postSet) {
        this.postSet = postSet;
    }

    public void addPostSet(Post post){
        this.postSet.add(post);
    }

    public Integer getNumberOfPosts(){
        return postSet.size();
    }

    public Integer getNumberOfComments(){
        return commentOfPostSet.size();
    }
}
