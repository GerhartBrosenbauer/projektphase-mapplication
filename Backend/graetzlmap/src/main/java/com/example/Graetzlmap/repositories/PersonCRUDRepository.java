package com.example.Graetzlmap.repositories;

import com.example.Graetzlmap.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonCRUDRepository extends CrudRepository<Person, Integer> {

    Person findByUsername(String username);
    Person findByUsernameAndPassword(String username, String password);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
    Person findByEmailAndPassword(String email, String password);
    void deleteByUsername(String username);
    Person findByEmail(String email);

}
