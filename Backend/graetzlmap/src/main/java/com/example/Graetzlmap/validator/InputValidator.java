package com.example.Graetzlmap.validator;
import com.example.Graetzlmap.dTOs.NewMapPostDTO;
import com.example.Graetzlmap.model.Person;
import com.example.Graetzlmap.repositories.CommentOfPostCRUDRepository;
import com.example.Graetzlmap.repositories.MapPostCRUDRepository;
import com.example.Graetzlmap.repositories.PersonCRUDRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Scope("singleton")
public class InputValidator {
    @Autowired
    PersonCRUDRepository personCRUDRepository;
    @Autowired
    MapPostCRUDRepository mapPostCRUDRepository;
    @Autowired
    CommentOfPostCRUDRepository commentOfPostCRUDRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    private static final InputValidator inputValidator = new InputValidator();

    private InputValidator(){}

    public static InputValidator getInstance(){
        return inputValidator;
    }

    //  TITLE               COMPLEX VALIDATIONS

    public void validateNewMapPost(@NotNull NewMapPostDTO newMapPostDTO){
        validateStringInternally(newMapPostDTO.getTitle());
        validateStringInternally(newMapPostDTO.getText());
        validatePostalCodeNotNull(newMapPostDTO.getPostalCode());
        validateLatitude(newMapPostDTO.getLatitude());
        validateLongitude(newMapPostDTO.getLongitude());
    }

    //  TITLE               VALIDATIONS THAT THROW EXCEPTIONS

    public void validateUsernameUniqueness(String username){
        if(personCRUDRepository.existsByUsername(username))
            throw new ResponseStatusException(HttpStatus.CONFLICT, valueAlreadyTaken("username"));
    }

    public void validateEmailUniqueness(String email){
        if(personCRUDRepository.existsByEmail(email) || !validateEmail(email))
            throw new ResponseStatusException(HttpStatus.CONFLICT, valueAlreadyTaken("email"));
    }

    private void validateStringInternally(String charArray){
        if(charArray.isEmpty() || charArray == null)
            throw new ResponseStatusException(HttpStatus.CONFLICT, "empty String");
    }

    public void validateMapPostExistence(Integer id){
        if(!mapPostCRUDRepository.existsById(id))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, valueNotFound("post"));
    }

    public void validatePostCommentExistence(Integer id){
        if(!commentOfPostCRUDRepository.existsById(id))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, valueNotFound("comment"));

    }

    public void validateUsername(String username){
        if(username.isEmpty() || username == null || !personCRUDRepository.existsByUsername(username))
            throw new ResponseStatusException(HttpStatus.CONFLICT, valueNotFound("username"));
    }

    public void validatePostalCodeNotNull(Integer postalCode){
       if(postalCode == null || !validatePostalCode(postalCode))
           throw new ResponseStatusException(HttpStatus.CONFLICT, valueOutOfBounds("postalCode"));
    }

    private void validateLatitude(Double latitude){
       if(latitude < 48.108715  || latitude > 48.324665)
           throw new ResponseStatusException(HttpStatus.CONFLICT, valueOutOfBounds("longitude"));
               }

    private void validateLongitude(Double longitude){
        if(longitude < 16.157424 || longitude > 16.587545)
            throw new ResponseStatusException(HttpStatus.CONFLICT, valueOutOfBounds("longitude"));
    }

    public void checkUsernameWithPassword(String username, String password){
        validateUsername(username);
        Person person = personCRUDRepository.findByUsername(username);
        System.out.println(passwordEncoder.encode(password));
        if(!passwordEncoder.matches(password, person.getPassword()))
            throw new ResponseStatusException(HttpStatus.CONFLICT, "password incorrect");

    }

    //  TITLE               VALIDATIONS THAT RETURN BOOLEANS

    public  boolean validatePassword(String password){
        if(password == null || password.isEmpty())
            return false;
        return password.length() >= 8 && password.length() <= 20;
    }

    public boolean validateEmail(String email){
        if(email == null || email.isEmpty())
            return false;
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean validatePostalCode(Integer postalCode){
        if(postalCode == null)
            return true;
        int start = 1010;
        int end = 1230;
        for(int i = start; i<= end; i+=10){
            if(i== postalCode) {
                return true;
            }
        }
        return false;
    }

    public boolean validatePasswordCheckIfEmpty(String password){
        return validatePassword(password) && !password.isEmpty() && password != null;
    }

    public boolean validateString(String charArray){
        return charArray != null && !charArray.equals("");
    }

    //  TITLE                   GETTERS

    private String valueNotFound(String value){
        return value += " not found.";
    }

    private String valueOutOfBounds(String value){
        return value += " out of bounds.";
    }

    private String valueAlreadyTaken(String value){
        return value += " already taken.";
    }

    public String getCurrentUsername(){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        validateUsername(username);
        return username;
    }
}

