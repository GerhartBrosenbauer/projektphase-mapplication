package com.example.Graetzlmap.dTOs;
import com.example.Graetzlmap.enums.Categories;

public class NewMapPostDTO {

    private String title;
    private String text;
    private Categories category;
    private Integer postalCode;
    private double latitude;
    private double longitude;

    public NewMapPostDTO(){}

    public NewMapPostDTO(String title, String text, Categories category, Integer postalCode, double latitude, double longitude) {
        this.title = title;
        this.text = text;
        this.category = category;
        this.postalCode = postalCode;
        this.latitude = latitude;
        this.longitude = longitude;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Categories getCategory() {
        return category;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}
