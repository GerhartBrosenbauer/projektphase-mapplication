package com.example.Graetzlmap.controller;
import com.example.Graetzlmap.dTOs.NewMapPostDTO;
import com.example.Graetzlmap.dTOs.UpdateMapPostDTO;
import com.example.Graetzlmap.enums.Roles;
import com.example.Graetzlmap.exitDTOs.MapPostWithCommentsDTO;
import com.example.Graetzlmap.model.MapPost;
import com.example.Graetzlmap.model.Person;
import com.example.Graetzlmap.repositories.MapPostCRUDRepository;
import com.example.Graetzlmap.repositories.PersonCRUDRepository;
import com.example.Graetzlmap.services.ImageUploadService;
import com.example.Graetzlmap.services.MapPostService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/mapPost")
public class MapPostController {
    @Autowired
    MapPostService mapPostService;
    @Autowired
    MapPostCRUDRepository mapPostCRUDRepository;
    @Autowired
    ImageUploadService imageUploadService;
    @Autowired
    PersonCRUDRepository personCRUDRepository;

    @PostMapping("")
    public Integer createNewMapPost(@RequestBody NewMapPostDTO newMapPostDTO) throws ResponseStatusException { return mapPostService.newMapPost(newMapPostDTO);}

    @GetMapping("/all")
    public List<MapPost> returnAllMapPosts(){return mapPostService.findAll();}

    @GetMapping("s/{username}")
    public List<MapPost> returnMapPostsOfUser(@PathVariable String username) throws ResponseStatusException {return mapPostService.findPostsByUser(username);}

   
    @PostMapping("/picture/{postId}")
    public void savePostPicture(@RequestParam ("image") @NotNull MultipartFile multipartFile, @PathVariable int postId) throws Exception{
        MapPost mapPost = mapPostCRUDRepository.findById(postId).get();
        Person person = personCRUDRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if(person.getRole() == Roles.ENDUSER)
            mapPost.setApprovalStatus(false);
        imageUploadService.savePostPicture(multipartFile, postId);
        mapPostCRUDRepository.save(mapPost);
    }

    @PutMapping("")
    public void putMapPost(UpdateMapPostDTO updateMapPostDTO) throws ResponseStatusException{ mapPostService.updateMapPost(updateMapPostDTO);}

    @DeleteMapping("/{id}")
    public List<MapPost> deleteSpecificMapPost(@PathVariable Integer id) throws ResponseStatusException{
        mapPostService.deleteMapPost(id);
        return mapPostService.findAllIllegalPosts();
    }
}
